package com.transmedia_technology.tonepositioning;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.*;
import android.os.Process;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;


public class MainActivity extends ActionBarActivity {

    private AudioRecord m_AudioRecord;
    private int m_SamplesRead;
    private int m_BufferSizeSamples;
    private int m_ChannelConfiguration = AudioFormat.CHANNEL_IN_MONO;
    private int m_AudioEncoding = AudioFormat.ENCODING_PCM_16BIT;
    private static short[] m_Buffer;
    private static final int SAMPPERSEC = 44100;
    private static final int m_BufferSec = 4;

    private TextView m_OutputTextView;
    private Button m_Button;
    private boolean m_IsRecording = false;

    private GraphView m_Graph;
    private LineGraphSeries<DataPoint> m_Series;
    private float m_volume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_OutputTextView = (TextView)findViewById(R.id.outputTextView);
        m_Button = (Button)findViewById(R.id.audioButton);
        m_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trigger();
            }
        });

        m_Graph = (GraphView)findViewById(R.id.graph);

        m_Series = new LineGraphSeries<DataPoint>();
        m_Graph.addSeries(m_Series);


        // Set up audio stuff
        int minBufferSize = AudioRecord.getMinBufferSize(SAMPPERSEC, m_ChannelConfiguration, m_AudioEncoding);
        m_BufferSizeSamples = m_BufferSec * SAMPPERSEC;
        if(minBufferSize > m_BufferSizeSamples) {
            m_BufferSizeSamples = minBufferSize;
        }

        m_Buffer = new short[m_BufferSizeSamples];
        m_AudioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPPERSEC, m_ChannelConfiguration, m_AudioEncoding, m_BufferSizeSamples * 2);

    }

    private void trigger() {
        acquire();
       // analyseAndDisplay();
    }

    private void acquire() {
        if(m_IsRecording)
            return;
        m_Button.setText("Recording...");
        m_IsRecording = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
                try {
                    m_AudioRecord.startRecording();
                    m_SamplesRead = m_AudioRecord.read(m_Buffer, 0, m_BufferSizeSamples);
                    Log.d("AudioRecord", "Samples Read : " + m_SamplesRead + " : " + m_BufferSizeSamples);
                    m_AudioRecord.stop();
                } catch (Throwable t) {
                    Log.e("AudioRecord", "Recording Failed");
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        m_Button.setText("Start Recording");
                        m_IsRecording = false;
                        analyseAndDisplay();
                    }
                });
            }
        }).start();
    }

    private void analyseAndDisplay() {

        float totalSquared = 0f;
        final int c_peakSampleBufferSize = 256;
        final float c_triggerThresholdUp = 0.1f;
        final float c_triggerThresholdDown = 0.05f;
        final int c_pulseTimeCount = 3;

        boolean aboveThreshold = false;

        float[] peakWaveformSamples = new float[c_peakSampleBufferSize];

        int initialPulseSample = -1;

        double[] pulseTimes = new double[c_pulseTimeCount];
        int[] pulseTimeSamples = new int[c_pulseTimeCount];

        int samplesPerPeak = m_Buffer.length / c_peakSampleBufferSize;

        // analyse the buffer

        int sampleCounter = 0;
        int i = 0;
        int j = 0;
        int pulseTimeCounter = 0;
        int bufferStart = 0;

        Log.d("AudioRecorder", "Scan SR:" + SAMPPERSEC + " : Samples Per Peak : " + samplesPerPeak  );

        for (int x = 0; i < m_Buffer.length; x++)
        {
            int pos = (bufferStart + x) % m_Buffer.length;
            float sample = (float)m_Buffer[pos] / Short.MAX_VALUE;
            float absSample = Math.abs(sample);

            // rms volume
            totalSquared += sample * sample;
            float meanSquared = totalSquared / m_Buffer.length;
            m_volume = (float)Math.sqrt((double)meanSquared);

            // threshold trigger - using the absSample for up and the peak value for down
            if (!aboveThreshold)
            {
                if (absSample > c_triggerThresholdUp)
                {
                    //Debug.Log("Trigger Up:" + sampleCounter );
                    aboveThreshold = true;
                    if (initialPulseSample == -1)
                    {
                        // we have not found the first pulse yet, this is it
                        //Debug.Log("Pulse First Detected");
                        initialPulseSample = sampleCounter;
                    }
                    else
                    {
                        // this is the next pulse, so work out the time and add it to the array
                        if (pulseTimeCounter < c_pulseTimeCount)
                        {
                            pulseTimeSamples[pulseTimeCounter] = sampleCounter - initialPulseSample;
                            pulseTimes[pulseTimeCounter] = (double)pulseTimeSamples[pulseTimeCounter] / (double)SAMPPERSEC * 1000.0;
                            pulseTimeCounter++;
                            Log.d("AudioRecorder", "Pulse " + pulseTimeCounter + ": " + pulseTimes[pulseTimeCounter - 1] + " msec - S:" + pulseTimeSamples[pulseTimeCounter - 1]);

                            if ( pulseTimeCounter == 3 ) {
                                // we have lift off

                                double mt1 = pulseTimes[0];
                                double mt2 = pulseTimes[1];
                                double mt3 = pulseTimes[2];

                                double x1 = (117649.0*mt2)/9200.0 - (117649.0*mt3)/9200.0 - (117649.0*mt2*mt3)/4600000.0 + (117649.0*Math.pow(mt2,2.0))/9200000.0 + (117649.0*Math.pow(mt3,2.0))/9200000.0 - (((343.0*mt2)/4600.0 - (343*mt3)/4600.0 + 1715.0/46.0)*(64038271157000000.0*mt1 + 12103178948000000.0*mt2 - 76141450105000000.0*mt3 + 1000.0*Math.pow((-(529.0*(343.0*mt2 - 343.0*mt3 + 166900.0)*(343.0*mt2 - 343.0*mt3 + 176100.0)*(343.0*mt1 - 343.0*mt3 + 339000.0)*(343.0*mt1 - 343.0*mt3 + 347000.0)*(117649.0*Math.pow(mt1,2.0) - 235298.0*mt1*mt2 + 117649000.0*mt1 + 117649.0*Math.pow(mt2,2.0) - 117649000.0*mt2 + 29375090000.0))/2500.0),(1.0/2.0)) - 128082348618000.0*mt1*mt3 - 48424328400000.0*mt2*mt3 + Math.pow(64041174309.0*mt1*mt3,2.0) - Math.pow(64041174309.0*mt1,2.0*mt3) + Math.pow(48424328400.0*mt2*mt3,2.0) - Math.pow(48424328400.0*mt2,2.0*mt3) + Math.pow(64041174309000.0*mt1,2.0) + Math.pow(21347058103.0*mt1,3.0) + Math.pow(24212164200000.0*mt2,2.0) + Math.pow(16141442800.0*mt2,3.0) + Math.pow(88253338509000.0*mt3,2.0) - Math.pow(37488500903.0*mt3,3.0) + 23360383725000000000.0))/(2000.0*(Math.pow(62236321.0*mt1,2.0) - 124472642.0*mt1*mt3 + 124472642000.0*mt1 + Math.pow(47059600.0*mt2,2.0) - 94119200.0*mt2*mt3 + 47059600000.0*mt2 + Math.pow(109295921.0*mt3,2.0) - 171532242000.0*mt3 + 73992757000000.0)) + 2943341.0/920.0;
                                double x2 = (117649.0*mt2)/9200.0 - (117649.0*mt3)/9200.0 - (117649.0*mt2*mt3)/4600000.0 + (117649.0*Math.pow(mt2,2.0))/9200000.0 + (117649.0*Math.pow(mt3,2.0))/9200000.0 - (((343.0*mt2)/4600.0 - (343*mt3)/4600.0 + 1715.0/46.0)*(64038271157000000.0*mt1 + 12103178948000000.0*mt2 - 76141450105000000.0*mt3 - 1000.0*Math.pow((-(529.0*(343.0*mt2 - 343.0*mt3 + 166900.0)*(343.0*mt2 - 343.0*mt3 + 176100.0)*(343.0*mt1 - 343.0*mt3 + 339000.0)*(343.0*mt1 - 343.0*mt3 + 347000.0)*(117649.0*Math.pow(mt1,2.0) - 235298.0*mt1*mt2 + 117649000.0*mt1 + 117649.0*Math.pow(mt2,2.0) - 117649000.0*mt2 + 29375090000.0))/2500.0),(1.0/2.0)) - 128082348618000.0*mt1*mt3 - 48424328400000.0*mt2*mt3 + Math.pow(64041174309.0*mt1*mt3,2.0) - Math.pow(64041174309.0*mt1,2.0*mt3) + Math.pow(48424328400.0*mt2*mt3,2.0) - Math.pow(48424328400.0*mt2,2.0*mt3) + Math.pow(64041174309000.0*mt1,2.0) + Math.pow(21347058103.0*mt1,3.0) + Math.pow(24212164200000.0*mt2,2.0) + Math.pow(16141442800.0*mt2,3.0) + Math.pow(88253338509000.0*mt3,2.0) - Math.pow(37488500903.0*mt3,3.0) + 23360383725000000000.0))/(2000.0*(Math.pow(62236321.0*mt1,2.0) - 124472642.0*mt1*mt3 + 124472642000.0*mt1 + Math.pow(47059600.0*mt2,2.0) - 94119200.0*mt2*mt3 + 47059600000.0*mt2 + Math.pow(109295921.0*mt3,2.0) - 171532242000.0*mt3 + 73992757000000.0)) + 2943341.0/920.0;

                                double y1 = (117649*mt1)/4000.0 - (117649.0*mt3)/4000.0 - (117649.0*mt1*mt3)/4000000.0 + (Math.pow(117649*mt1,2.0))/8000000.0 + (Math.pow(117649.0*mt3,2.0))/8000000.0 - (((343.0*mt1)/4000.0 - (343.0*mt3)/4000.0 + 343.0/4.0)*(64038271157000000.0*mt1 + 12103178948000000.0*mt2 - 76141450105000000.0*mt3 + 1000.0*Math.pow((-(529.0*(343.0*mt2 - 343.0*mt3 + 166900.0)*(343.0*mt2 - 343.0*mt3 + 176100.0)*(343.0*mt1 - 343.0*mt3 + 339000.0)*(343.0*mt1 - 343.0*mt3 + 347000.0)*(Math.pow(117649.0*mt1,2.0) - 235298.0*mt1*mt2 + 117649000.0*mt1 + Math.pow(117649.0*mt2,2.0) - 117649000.0*mt2 + 29375090000.0))/2500.0),(1.0/2.0)) - 128082348618000.0*mt1*mt3 - 48424328400000.0*mt2*mt3 + Math.pow(64041174309.0*mt1*mt3,2.0) - Math.pow(64041174309.0*mt1,2.0*mt3) + Math.pow(48424328400.0*mt2*mt3,2.0) - Math.pow(48424328400.0*mt2,2.0*mt3) + Math.pow(64041174309000.0*mt1,2.0) + Math.pow(21347058103.0*mt1,3.0) + Math.pow(24212164200000.0*mt2,2.0) + Math.pow(16141442800.0*mt2,3.0) + Math.pow(88253338509000.0*mt3,2.0) - Math.pow(37488500903.0*mt3,3.0) + 23360383725000000000.0))/(2000.0*(Math.pow(62236321.0*mt1,2.0) - 124472642.0*mt1*mt3 + 124472642000.0*mt1 + Math.pow(47059600.0*mt2,2.0) - 94119200.0*mt2*mt3 + 47059600000.0*mt2 + Math.pow(109295921.0*mt3,2.0) - 171532242000.0*mt3 + 73992757000000.0)) + 117665.0/8.0;
                                double y2 = (117649*mt1)/4000.0 - (117649.0*mt3)/4000.0 - (117649.0*mt1*mt3)/4000000.0 + (Math.pow(117649*mt1,2.0))/8000000.0 + (Math.pow(117649.0*mt3,2.0))/8000000.0 - (((343.0*mt1)/4000.0 - (343.0*mt3)/4000.0 + 343.0/4.0)*(64038271157000000.0*mt1 + 12103178948000000.0*mt2 - 76141450105000000.0*mt3 - 1000.0*Math.pow((-(529.0*(343.0*mt2 - 343.0*mt3 + 166900.0)*(343.0*mt2 - 343.0*mt3 + 176100.0)*(343.0*mt1 - 343.0*mt3 + 339000.0)*(343.0*mt1 - 343.0*mt3 + 347000.0)*(Math.pow(117649.0*mt1,2.0) - 235298.0*mt1*mt2 + 117649000.0*mt1 + Math.pow(117649.0*mt2,2.0) - 117649000.0*mt2 + 29375090000.0))/2500.0),(1.0/2.0)) - 128082348618000.0*mt1*mt3 - 48424328400000.0*mt2*mt3 + Math.pow(64041174309.0*mt1*mt3,2.0) - Math.pow(64041174309.0*mt1,2.0*mt3) + Math.pow(48424328400.0*mt2*mt3,2.0) - Math.pow(48424328400.0*mt2,2.0*mt3) + Math.pow(64041174309000.0*mt1,2.0) + Math.pow(21347058103.0*mt1,3.0) + Math.pow(24212164200000.0*mt2,2.0) + Math.pow(16141442800.0*mt2,3.0) + Math.pow(88253338509000.0*mt3,2.0) - Math.pow(37488500903.0*mt3,3.0) + 23360383725000000000.0))/(2000.0*(Math.pow(62236321.0*mt1,2.0) - 124472642.0*mt1*mt3 + 124472642000.0*mt1 + Math.pow(47059600.0*mt2,2.0) - 94119200.0*mt2*mt3 + 47059600000.0*mt2 + Math.pow(109295921.0*mt3,2.0) - 171532242000.0*mt3 + 73992757000000.0)) + 117665.0/8.0;

                                double slt1 = 1500.0 - (64038271157000000.0*mt1 + 12103178948000000.0*mt2 - 76141450105000000.0*mt3 + 1000.0*Math.pow((-(529.0*(343.0*mt2 - 343.0*mt3 + 166900.0)*(343.0*mt2 - 343.0*mt3 + 176100.0)*(343.0*mt1 - 343.0*mt3 + 339000.0)*(343.0*mt1 - 343.0*mt3 + 347000.0)*(Math.pow(117649.0*mt1,2.0) - 235298.0*mt1*mt2 + 117649000.0*mt1 + Math.pow(117649.0*mt2,2.0) - 117649000.0*mt2 + 29375090000.0))/2500.0),(1.0/2.0)) - 128082348618000.0*mt1*mt3 - 48424328400000.0*mt2*mt3 + Math.pow(64041174309.0*mt1*mt3,2.0) - Math.pow(64041174309.0*mt1,2.0*mt3) + Math.pow(48424328400.0*mt2*mt3,2.0) - Math.pow(48424328400.0*mt2,2.0*mt3) + Math.pow(64041174309000.0*mt1,2.0) + Math.pow(21347058103.0*mt1,3.0) + Math.pow(24212164200000.0*mt2,2.0) + Math.pow(16141442800.0*mt2,3.0) + Math.pow(88253338509000.0*mt3,2.0) - Math.pow(37488500903.0*mt3,3.0) + 23360383725000000000.0)/(686.0*(Math.pow(62236321.0*mt1,2.0) - 124472642.0*mt1*mt3 + 124472642000.0*mt1 + Math.pow(47059600.0*mt2,2.0) - 94119200.0*mt2*mt3 + 47059600000.0*mt2 + Math.pow(109295921.0*mt3,2.0) - 171532242000.0*mt3 + 73992757000000.0)) - mt3;
                                double slt2 = 1500.0 - (64038271157000000.0*mt1 + 12103178948000000.0*mt2 - 76141450105000000.0*mt3 - 1000.0*Math.pow((-(529.0*(343.0*mt2 - 343.0*mt3 + 166900.0)*(343.0*mt2 - 343.0*mt3 + 176100.0)*(343.0*mt1 - 343.0*mt3 + 339000.0)*(343.0*mt1 - 343.0*mt3 + 347000.0)*(Math.pow(117649.0*mt1,2.0) - 235298.0*mt1*mt2 + 117649000.0*mt1 + Math.pow(117649.0*mt2,2.0) - 117649000.0*mt2 + 29375090000.0))/2500.0),(1.0/2.0)) - 128082348618000.0*mt1*mt3 - 48424328400000.0*mt2*mt3 + Math.pow(64041174309.0*mt1*mt3,2.0) - Math.pow(64041174309.0*mt1,2.0*mt3) + Math.pow(48424328400.0*mt2*mt3,2.0) - Math.pow(48424328400.0*mt2,2.0*mt3) + Math.pow(64041174309000.0*mt1,2.0) + Math.pow(21347058103.0*mt1,3.0) + Math.pow(24212164200000.0*mt2,2.0) + Math.pow(16141442800.0*mt2,3.0) + Math.pow(88253338509000.0*mt3,2.0) - Math.pow(37488500903.0*mt3,3.0) + 23360383725000000000.0)/(686.0*(Math.pow(62236321.0*mt1,2.0) - 124472642.0*mt1*mt3 + 124472642000.0*mt1 + Math.pow(47059600.0*mt2,2.0) - 94119200.0*mt2*mt3 + 47059600000.0*mt2 + Math.pow(109295921.0*mt3,2.0) - 171532242000.0*mt3 + 73992757000000.0)) - mt3;

                                Log.d("AudioRecorder", "X1:" + x1 + " X2:" + x2 );
                                Log.d("AudioRecorder", "Y1:" + y1 + " Y2:" + y2 );
                                Log.d("AudioRecorder", "T1:" + slt1 + " T2:" + slt2 );

                                // set the object position on the screen

                            }

                        }
                        else
                        {
                            Log.d("AudioRecorder", "Too many pulses - ignoring");
                        }
                    }
                }
            }



            // peakWaveform Calculations
            if (i++ < samplesPerPeak)
            {
                // check this sample and add to the peak array if it's bigger than what we have already stored
                if (absSample > peakWaveformSamples[j])
                {
                    peakWaveformSamples[j] = absSample;
                }
            }
            else
            {
                if(aboveThreshold)
                {
                    if (peakWaveformSamples[j] < c_triggerThresholdDown)
                    {
                        //Debug.Log("Trigger Down:" + sampleCounter );
                        aboveThreshold = false;
                    }
                }
                // we are done, move along
                //Debug.Log("Peak[" + j + "] = " + peakWaveformSamples[j] );
                j++;
                i = 0;
            }
            if (j >= c_peakSampleBufferSize)
            {
                // not sure how, but in case we have some rounding issues...
                break;
            }

            sampleCounter++;
        }

        DataPoint[] points = new DataPoint[peakWaveformSamples.length];
        for(int k = 0; k < peakWaveformSamples.length; k++)
        {
            points[k] = new DataPoint(k, peakWaveformSamples[k]);
        }

        m_Series.resetData(points);

//        m_OutputTextView.setText("buffersizebytes " + m_BufferSizeSamples + "\n");
//        for(int l = 0; l < 256; l++) {
//            m_OutputTextView.append(" " + m_Buffer[l]);
//        }
//        m_OutputTextView.invalidate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(m_AudioRecord != null) {
            m_AudioRecord.stop();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(m_AudioRecord != null) {
            m_AudioRecord.release();
        }
    }
}
